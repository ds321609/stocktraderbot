﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace MarketDataService.Utilities
{
    public class JReader : JsonTextReader
    {
        public JReader(TextReader r) : base(r) 
        {
        
        }

        public override bool Read()
        {
            bool b = base.Read();
            if (base.CurrentState == State.Property)
            {
                ReplaceParenthesisAndContentInside();
                ReplaceOrderedListingsInKey();
                KeyContainsSpaces();
                
            }
            return b;
        }

        public void ReplaceParenthesisAndContentInside()
        {
            base.SetToken(JsonToken.PropertyName, Regex.Replace(((string)base.Value), @"\s\(.+\)", string.Empty));
        }
        public void ReplaceOrderedListingsInKey()
        {
            base.SetToken(JsonToken.PropertyName, Regex.Replace(((string)base.Value), @"[0-9]+\.\s", string.Empty));
        }
        private void KeyContainsSpaces()
        {
            DateTime datetime;
            var isDateTime = DateTime.TryParse((string)base.Value, out datetime);

            if(!isDateTime) base.SetToken(JsonToken.PropertyName, ((string)base.Value).Replace(" ", "_"));
        }
    }
}
