﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MarketDataService
{
    public class TimeSeries
    {
        [JsonProperty("Meta Data")]
        private MetaData Metadata { get; }
        List<Period> Periods { get; }
        
        public TimeSeries(List<Period> periods, DateTime lastRefreshed)
        {
            Periods = periods.OrderBy(x => x.Timestamp).ToList();
            Metadata = new MetaData();
            Metadata.LastRefreshed = lastRefreshed;
        }
        public TimeSeries(List<Period> periods)
        {
            Periods = periods.OrderBy(x => x.Timestamp).ToList();
        }
        public TimeSeries(string json)
        {
            Periods = new List<Period>();
            Metadata = JObject.Parse(json).SelectToken("Meta_Data").ToObject<MetaData>();

            var periodData = JToken.Parse(json).SelectToken("Time_Series");

            foreach(JProperty period in periodData)
            {
                DateTime timestamp = DateTime.Parse(period.Name);
                Period convert = period.First.ToObject<Period>();
                convert.Timestamp = timestamp;
                Periods.Add(convert);

            }

            Periods = Periods.OrderBy(x => x.Timestamp).ToList();
        }

        public List<Period> GetIntraDayToday() 
        {
            DateTime today = DateTime.Now.Date;

            return Periods.FindAll(x => x.Timestamp.Date == today);
        }

        public List<Period> GetPeriods()
        {
            return Periods.OrderBy(x => x.Timestamp).ToList();
        }

        public DateTime GetLastRefreshed()
        {
            return Metadata.LastRefreshed;
        }

        public Period GetLatestPeriod()
        {
            return Periods.Last();
        }

        public string GetTickerSymbol()
        {
            return Metadata.Symbol;
        }

    }

    public class MetaData
    {
        public string Information { get; set; }
        public string Symbol { get; set; }
        [JsonProperty("Last_Refreshed")]
        public DateTime LastRefreshed { get; set; }
        public string Interval { get; set; }
        [JsonProperty("Output_Size")]
        public string OutputSize { get; set; }
        public string Zone { get; set; }
    }

    public class Period
    {
            public DateTime Timestamp { get; set; }
            public double Open { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Close { get; set; }
            public long Volume { get; set; }
    }
}
