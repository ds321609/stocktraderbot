﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace MarketDataService
{
    public class GlobalQuote
    {
        [JsonProperty("symbol")]
        public string TickerSymbol { get; set; }
        public DateTime Timestamp { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        [JsonProperty("price")]
        public double CurrentPrice { get; set; }
        public double Volume { get; set; }
        [JsonProperty("previous_close")]
        public double Close { get; set; }
        [JsonProperty("change")]
        public double Change { get; set; }

    }
}
