﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarketDataService
{
    public class Criterion
    {
        public string field { get; set; }
        public List<string> operators { get; set; }
        public List<object> values { get; set; }
        public List<object> labelsSelected { get; set; }
    }

    public class CriteriaMeta
    {
        public int size { get; set; }
        public int offset { get; set; }
        public string sortField { get; set; }
        public string sortType { get; set; }
        public string quoteType { get; set; }
        public string topOperator { get; set; }
        public List<Criterion> criteria { get; set; }
    }

    public class Quote
    {
        public string language { get; set; }
        public string region { get; set; }
        public string quoteType { get; set; }
        public bool triggerable { get; set; }
        public int exchangeDataDelayedBy { get; set; }
        public int sourceInterval { get; set; }
        public string exchangeTimezoneName { get; set; }
        public string exchangeTimezoneShortName { get; set; }
        public int gmtOffSetMilliseconds { get; set; }
        public bool esgPopulated { get; set; }
        public bool tradeable { get; set; }
        public string market { get; set; }
        public string marketState { get; set; }
        public string exchange { get; set; }
        public int priceHint { get; set; }
        public string fullExchangeName { get; set; }
        public string symbol { get; set; }
    }

    public class Result
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string canonicalName { get; set; }
        public CriteriaMeta criteriaMeta { get; set; }
        public string rawCriteria { get; set; }
        public int start { get; set; }
        public int count { get; set; }
        public int total { get; set; }
        public List<Quote> quotes { get; set; }
        public bool predefinedScr { get; set; }
        public int versionId { get; set; }
    }

    public class Finance
    {
        public List<Result> result { get; set; }
        public object error { get; set; }
    }

    public class RootObject
    {
        public Finance finance { get; set; }
    }
}
