﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MarketDataService;

namespace MarketDataService
{
    public interface IMarketDataAdapter
    {
        TimeSeries GetStockIntraDay(string ticker, bool fullSize);
        GlobalQuote GetGlobalQuote(string ticker);
        List<string> GetTopGainers();
    }
}
