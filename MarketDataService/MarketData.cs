﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading.Tasks;
using MarketDataService.Utilities;
using System.IO;

namespace MarketDataService
{
    public class MarketData : IMarketDataAdapter
    {
        HttpClient _client;
        string _alphaVantageApiKey;
        string _yahooFinApiKey;

        string apiHostUri = "https://www.alphavantage.co/query?";
        

        public MarketData()
        {
            _client = new HttpClient();
            _alphaVantageApiKey = Environment.GetEnvironmentVariable("ALPHA_VANTAGE_API_KEY");
            _yahooFinApiKey = Environment.GetEnvironmentVariable("YAHOO_FINANCE_API_KEY");
        }

        public MarketData(HttpClient client, string apiKey)
        {
            _client = client;
            _alphaVantageApiKey = apiKey;
            _yahooFinApiKey = apiKey;
        }

        public TimeSeries GetStockIntraDay(string ticker, bool fullSize)
        {
            try
            {

                var full = fullSize ? "&outputsize=full" : "&outputsize=compact";
                
                var endpoint = $"{apiHostUri}function=TIME_SERIES_INTRADAY&symbol={ticker}&interval=1min{full}&apikey={_alphaVantageApiKey}";



                var response = _client.GetAsync(endpoint)
                                    .Result;
                                    
                var content = response.Content
                                    .ReadAsStringAsync()
                                    .Result;

                string data = DeseralizeWithRules(content);
                string cleanUp = CleanupIntraDayResponse(data);

                if (IsErrorResponse(cleanUp))
                {
                    throw RaiseResponseError(ticker, cleanUp, endpoint);
                }
                else
                {
                    return new TimeSeries(cleanUp);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GlobalQuote GetGlobalQuote(string ticker)
        {
            try
            {
                var endpoint = $"{apiHostUri}function=GLOBAL_QUOTE&symbol={ticker}&apikey={_alphaVantageApiKey}";
                var response = _client.GetAsync(endpoint)
                                    .Result
                                    .Content
                                    .ReadAsStringAsync()
                                    .Result;

                string data = DeseralizeWithRules(response);
                string cleanUp = CleanupIntraDayResponse(data);

                if (IsErrorResponse(cleanUp))
                {
                    throw RaiseResponseError(ticker, cleanUp, endpoint);
                }
                else
                {
                    return ExtractPeriod(cleanUp);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetTopGainers()
        {
            try
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri("https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/get-movers"),
                    Method = HttpMethod.Get
                };

                request.Headers.Add("x-rapidapi-host", "apidojo-yahoo-finance-v1.p.rapidapi.com");
                request.Headers.Add("x-rapidapi-key", _yahooFinApiKey);

                var response = _client.SendAsync(request)
                                    .Result;
                                    

                if (IsErrorResponse(response))
                {
                    throw new Exception($"Failed to retrieve market data: Status ${response.StatusCode}: ${response.ReasonPhrase}");
                }

                var responseContent = response.Content
                                    .ReadAsStringAsync()
                                    .Result;

                var convertResponse = JsonConvert.DeserializeObject<RootObject>(responseContent);

                

                return convertResponse.finance.result[0].quotes.Select(x => x.symbol).ToList();


            } catch(Exception ex)
            {
                throw ex;
            }
        }

        private static Exception RaiseResponseError(string ticker, string cleanUp, string queryString)
        {
            var error = JObject.Parse(cleanUp).First.Values().FirstOrDefault().ToString();

            throw new Exception($"No intraday stock data could be retrieved for {ticker} using: \n {queryString}",
                new Exception(error));
        }

        private static string DeseralizeWithRules(string json)
        {
            JsonSerializer serializer = new JsonSerializer();
            var jObj = serializer.Deserialize(new JReader(new StringReader(json))) as JObject;

            return jObj.ToString(Newtonsoft.Json.Formatting.None);


        }

        private bool IsErrorResponse(HttpResponseMessage response)
        {
            return !response.IsSuccessStatusCode;
        }

        private static bool IsErrorResponse(string cleanUp)
        {
            var parsedObject = JObject.Parse(cleanUp);

            return parsedObject.ContainsKey("Error_Message") || parsedObject.ContainsKey("Note");
        }

        private static GlobalQuote ExtractPeriod(string cleanUp)
        {
            var json = JObject.Parse(cleanUp).SelectToken("Global_Quote");
            return JsonConvert.DeserializeObject<GlobalQuote>(json.ToString());
        }

        private static string CleanupIntraDayResponse(string response)
        {
            string cleanUp = response;
            //Regex.Replace(response, @"[0-9]+.\s+:", string.Empty);
            cleanUp = cleanUp.Replace("\n    ", string.Empty);
            cleanUp = cleanUp.Replace("\n        ", string.Empty);
            cleanUp = cleanUp.Replace(": ", ":");
            cleanUp = cleanUp.Replace("    ", string.Empty);
            //cleanUp = cleanUp.Replace(' ', '_');
            return cleanUp;
        }
    }
}
