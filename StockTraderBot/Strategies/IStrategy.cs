﻿using Alpaca.Markets;
using System;
using System.Collections.Generic;
using System.Text;

namespace StockTraderBot.Strategies
{
    public interface IStrategy
    {
        public bool BuyingRules(Position position);
        public bool SellingRules(Position position);
    }
}


