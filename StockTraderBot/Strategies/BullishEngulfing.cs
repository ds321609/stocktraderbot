﻿using Alpaca.Markets;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MarketDataService;
using StockTraderBot.Technical_Indicators;

namespace StockTraderBot.Strategies
{
    public class BullishEngulfing : IStrategy
    {
        public bool BuyingRules(Position position)
        {
            var timeSeries = position.IntraDayTrack.GetPeriods();
            var trend = position.Average.GetTrend();

            var currentPeriod = timeSeries.LastOrDefault();
            var previousPeriod = timeSeries.TakeLast(2).FirstOrDefault();

            if (timeSeries.Count < 1) return false;

            return trend < -0.1 ? IsBullishEngulfing(currentPeriod, previousPeriod) : false;

        }

        public bool SellingRules(Position position)
        {
            var closePrice = position.IntraDayTrack.GetPeriods().Last().Close;
            var trailingStop = position.TrailingStopPrice;

            return closePrice <= trailingStop;
        }

        private bool IsBullishEngulfing(Period currentPeriod, Period previousPeriod)
        {
            var currentOpen = currentPeriod.Open;
            var previousOpen = previousPeriod.Open;
            var currentClose = currentPeriod.Close;
            var previousClose = previousPeriod.Close;

            return (currentClose >= previousOpen && currentOpen >= previousClose) ||
                (currentClose >= previousOpen && currentOpen > previousClose) ||
                (currentClose > previousOpen && currentOpen >= previousClose);
        }
    }
}
