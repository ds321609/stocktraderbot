﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MarketDataService;
using StockTraderBot.Technical_Indicators;

namespace StockTraderBot
{
    public class Position
    {
        public string TickerSymbol { get; set; }
        public string Name { get; set; }
        public double TrailingStopPrice { get; set; } = 0.0;
        public long Shares { get; set; }
        public DateTime LastRefreshed { get; set; }
        public TimeSeries IntraDayTrack { get; set; }
        public double TradableCashBalance { get; set; }
        public SimpleMovingAverage Average { get; set; }
        public bool NeedsAnalyzed { get; set; }

        public Position()
        {
            IntraDayTrack = new TimeSeries(new List<Period>());
            LastRefreshed = DateTime.Now;
            Average = new SimpleMovingAverage();
        }
        public bool UpdatePosition(TimeSeries updatedTimeseries) 
        {
            List<Period> periods = updatedTimeseries.GetPeriods();
            DateTime recentRefresh = updatedTimeseries.GetLastRefreshed();
            var close = periods.Last().Close;

            if(LastRefreshed != recentRefresh)
            {
                IntraDayTrack = updatedTimeseries;
                Average.UpdateSMA(IntraDayTrack.GetPeriods());
                UpdateTrailingStop(close);
                LastRefreshed = recentRefresh;
                NeedsAnalyzed = true;

                return true;
            }

            return false;
        }

        private void UpdateTrailingStop(double close)
        {
            var fixedPrice = close - (close * .02);
            var closeIsHigher = close > TrailingStopPrice;

            if (closeIsHigher) TrailingStopPrice = fixedPrice; 
        }
    }
}
