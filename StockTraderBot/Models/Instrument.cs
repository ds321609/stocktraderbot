﻿using Serilog;
using StockTraderBot.Models;
using StockTraderBot.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace StockTraderBot
{
    public static class Instrument
    {
        public static TradeAction AnalyzePosition(Position position)
        {
            try
            {
                BullishEngulfing bullishEngulfing = new BullishEngulfing();
                bool buy = bullishEngulfing.BuyingRules(position);
                bool sell = bullishEngulfing.SellingRules(position);

                if (position.Shares == 0 && buy)
                {
                    return new TradeAction(TradeActionType.Buy, GetQuanityFromRisk(position), position.TickerSymbol);
                }

                if (sell)
                {
                    return new TradeAction(TradeActionType.Sell, position.Shares, position.TickerSymbol);
                }

                return new TradeAction(TradeActionType.Wait, 0, position.TickerSymbol);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static long GetQuanityFromRisk(Position position)
        {
            return (long)(Math.Truncate(position.TradableCashBalance * .1) / position.IntraDayTrack.GetLatestPeriod().Close);
        }
        private static void ApplyStrategy(Position position)
        {
            throw new NotImplementedException();
        }
    }
}
