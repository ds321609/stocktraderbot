﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockTraderBot.Models
{
    public class TradeAction
    {
        public TradeActionType Action { get; }
        public long Shares { get; }
        public string TickerSymbol { get; }
        public TradeAction(TradeActionType type, long shares, string ticker)
        {
            Action = type;
            Shares = shares;
            TickerSymbol = ticker;
        }
    }

    public enum TradeActionType
    {
        Buy = 0,
        Sell = 1,
        Wait = 2
    }
}
