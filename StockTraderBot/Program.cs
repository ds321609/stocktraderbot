﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using Autofac;
using Serilog;

namespace StockTraderBot
{
    class Program
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Environment.Exit(Environment.ExitCode);

            return true;
        }
        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            try
            {
                var container = ContainerConfig.Configure();

                using (var scope = container.BeginLifetimeScope())
                {
                    var app = scope.Resolve<IApplication>();
                    app.Run();
                }

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The application failed to start up");
            } 
            finally
            {
                Log.CloseAndFlush();
            }
           
        }

        private void ManualWindowClose(object sender, EventArgs e)
        {

        }
    }
}
