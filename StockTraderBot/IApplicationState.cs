﻿using System;
using System.Collections.Generic;
using Alpaca.Markets;
using MarketDataService;

namespace StockTraderBot
{
    public interface IApplicationState
    {
        Clock MarketHours { get; set; }
        List<Position> Positions { get; set; }
        List<string> TrackedStocks { get; set; }
        void AddPosition(Position position);
        void SetPosition(Position position);
        Position GetPosition(string ticker);
        List<Position> GetCurrentPositions();
        void ResetPositions();
        void UpdateIntraDayData(TimeSeries data);
        void SetTrackedStocks(List<string> tickers); 
    }
}