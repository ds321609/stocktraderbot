﻿using MarketDataService;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace StockTraderBot.Technical_Indicators
{
    public class SimpleMovingAverage : IMovingAverage
    {
        public double[] _averages;

        public SimpleMovingAverage() {
            _averages = new double[0];
        }
        public SimpleMovingAverage(double[] averages)
        {
            _averages = averages;
        }

        public double[] UpdateSMA(List<Period> intraday)
        {
            double newAverage = GetNewAverage(intraday);
            PushNewAverage(newAverage);
            return _averages;
        }

        public void SetSMAFromTimeSeriesData(int days, TimeSeries timeSeries)
        {
            List<Period> currentPeriods = timeSeries.GetPeriods();

            for (int i = 0; i < days; i++)
            {
                int fromLastIndex = (currentPeriods.Count + 1) - (days - i) - days;
                List<Period> meanValues = currentPeriods.GetRange(fromLastIndex, days);

                PushNewAverage(GetNewAverage(meanValues));
            }
        }

        private static double GetNewAverage(List<Period> intraday)
        {
            var getLastPeriods = intraday.TakeLast(5);
            return Math.Round(getLastPeriods.Aggregate(0.0, (acc, x) => acc + x.Close) / getLastPeriods.Count(), 6);
        }

        public double GetTrend()
        {
            var first = _averages.First();
            var last = _averages.Last();

            return (last - first) / _averages.Length;
        }

        private void PushNewAverage(double newAverage)
        {
            Array.Resize(ref _averages, _averages.Length + 1);
            int index = Array.IndexOf(_averages, 0);

            if (index != -1)
            {
                _averages[index] = newAverage;
            }
        }
    }

    public interface IMovingAverage
    {
        public double[] UpdateSMA(List<Period> intraday);
    }
}
