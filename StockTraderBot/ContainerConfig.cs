﻿using Autofac;
using AutofacSerilogIntegration;
using Serilog;
using ExchangeService;
using ExchangeService.Adapters;
using MarketDataService;
using StockTraderBot.Technical_Indicators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Serilog.Sinks.SystemConsole.Themes;
using Microsoft.Extensions.Configuration;

namespace StockTraderBot
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            Log.Information("Initializing the broker bot...");

            builder.RegisterLogger();
            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterType<ApplicationState>().As<IApplicationState>();
            builder.RegisterType<MarketData>().As<IMarketDataAdapter>();
            builder.RegisterType<Exchange>().As<IExchangeAdapter>();
            builder.RegisterType<AlpacaRestClient>().As<IAlpacaRestClientAdapter>();

            return builder.Build();
        }
    }
}
