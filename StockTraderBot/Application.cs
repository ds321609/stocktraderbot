﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Alpaca.Markets;
using ExchangeService.Adapters;
using MarketDataService;
using Serilog;
using StockTraderBot.Models;

namespace StockTraderBot
{
    public class Application : IApplication
    {
        IExchangeAdapter _exchange;
        IMarketDataAdapter _marketData;
        IApplicationState _appState;
        ILogger _logger;

        static Timer sessionIntervalTimer;

        public Application(IExchangeAdapter exchange, IMarketDataAdapter marketData, IApplicationState appState, ILogger logger)
        {
            _exchange = exchange;
            _marketData = marketData;
            _appState = appState;

            if (logger == null) throw new ArgumentNullException("log");
            _logger = logger;

        }

        public void Run()
        {
            _logger.Information("Starting the broker bot...");

            SetMarketHours();

            _logger.Information("Set market hours open:{OpenTime} and close:{CloseTime}", _appState.MarketHours.NextOpen, _appState.MarketHours.NextClose);

            sessionIntervalTimer = new Timer()
            {
                Interval = 60000,
                Enabled = true
            };
            sessionIntervalTimer.Elapsed += SessionEvent;
        }

        private void SessionEvent(object source, EventArgs e)
        {
            var currentDateTime = DateTime.Now;
            SessionHandler(currentDateTime);
        }

        public void SessionHandler(DateTime current)
        {
            try
            {
                var positions = GetWatchedPositions().ToList();

                var marketIsOpen = CheckMarketHours(current);

                if (!marketIsOpen)
                {
                    return;
                }

                if (marketIsOpen && positions.Count() < 5)
                {
                    PopulatePositionsFromTopGainers();
                }

                _logger.Debug("Updating positions for next session");

                UpdateForSession(GetWatchedPositions().ToList());

                _logger.Debug("Analyzing positions with instruments");

                AnalyzePositionsForSession();

                _logger.Debug("Analyzation is complete. Awaiting next session");

            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.Message);
            }
        }

        public void UpdateForSession(List<Position> positions)
        {
            
            foreach (Position position in positions)
            {
                try
                {
                    _logger.Debug("requesting time series data for: {Ticker}", position.TickerSymbol);
                    var timeseries = _marketData.GetStockIntraDay(position.TickerSymbol, false);
                    _logger.Debug("Pulling market period data from {Symbol}", position.TickerSymbol);
                    UpdateIntraDayData(timeseries);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "{ExceptionMessage} | {InnerException}", ex.Message, ex.InnerException);
                }
            }
            
        }

        public bool CheckMarketHours(DateTime current)
        {
            if (CheckMarketIsClosed(current)) return false;

            if (IsMarketNearToClose(current)) return false;

            return true;
        }

        public bool CheckMarketIsClosed(DateTime current)
        {
            if (IsMarketClosed(current))
            {
                UpdateNextTradingDayHours(current);
                return true;
            }
            return false;
        }

        private bool IsMarketNearToClose(DateTime current)
        {
            var positions = _appState.GetCurrentPositions();
            bool nearClosing = current.AddMinutes(2) > _appState.MarketHours.NextClose;

            if (nearClosing)
            {
                Log.Information("Market will be closing very soon. Selling off securities");
                ExitPositionsBeforeClose(positions);
                return true;
            }

            return false;

        }

        private bool IsMarketClosed(DateTime current)
        {
            return current <= _appState.MarketHours.NextOpen || current >= _appState.MarketHours.NextClose;
        }

        private void ExitPositionsBeforeClose(List<Position> positions)
        {
            ExitAllTrades(positions);
            ClearPositions();
            _logger.Information("Cleared positions for next trading day. Closing down...");
            Environment.Exit(0);
        }

        private void ExitAllTrades(List<Position> positions)
        {
            foreach (Position position in positions.ToList())
            {
                try
                {
                    var quantity = position.Shares;
                    var ticker = position.TickerSymbol;

                    if (position.Shares != 0)
                    {
                        CommitTrade(new TradeAction(TradeActionType.Sell, quantity, ticker));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Failed to submit order of {Shares} of {TickerOfStock}", position.Shares, position.TickerSymbol);
                }
            }
        }

        private void UpdateNextTradingDayHours(DateTime current)
        {
            if (_appState.MarketHours.NextOpen < current)
            {
                _logger.Information("The market is closed right now. It will open on {NextMarketOpenTime}", _appState.MarketHours.NextOpen);
                _logger.Debug("New open and close times: {Open} and {Close}", _appState.MarketHours.NextOpen, _appState.MarketHours.NextClose);
                SetMarketHours();
            }
        }

        public void UpdateIntraDayData(TimeSeries data)
        {
            SetPosition(data);
        }

        public void AnalyzePositionsForSession()
        {
            var positions = _appState.GetCurrentPositions().ToList();

            foreach (var pos in positions)
            {
                AnalyzePosition(pos);
            }
        }

        public void AnalyzePosition(Position position)
        {
            try
            {
                if(position.NeedsAnalyzed)
                {
                    TradeAction action = Instrument.AnalyzePosition(position);
                
                    if(action.Action != TradeActionType.Wait) CommitTrade(action);

                    SetIsAnalyzed(position.TickerSymbol);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Position {Symbol} failed to be processed", position.TickerSymbol);
            }
        }

        private void CommitTrade(TradeAction tradeAction)
        {
            try
            {
                IOrder order = tradeAction.Action == TradeActionType.Buy ? _exchange.PurchaseStockAtMarket(tradeAction.TickerSymbol, tradeAction.Shares) :
                    _exchange.SellStockAtMarket(tradeAction.TickerSymbol, tradeAction.Shares);

                LogOrder(order);

                if (OrderStatusFailed(order))
                {
                    return;
                }

                SetPosition(tradeAction);

            }
            catch (Exception ex)
            {
                _logger.Error(ex, "{Symbol} Failed to complete trade", tradeAction.TickerSymbol);
            }
        }

        private static bool OrderStatusFailed(IOrder order)
        {
            return order.OrderStatus == OrderStatus.Rejected ||
                                order.OrderStatus == OrderStatus.Stopped ||
                                order.OrderStatus == OrderStatus.Suspended ||
                                order.OrderStatus == OrderStatus.Expired;
        }

        public void PopulatePositionsFromTopGainers()
        {
            List<string> topGainers = GetTrackedStocks();
            var portionedTradableCash = (double)(_exchange.GetAvailableCash()) / topGainers.Count;

            foreach (string ticker in topGainers)
            {
                var position = new Position()
                {
                    TickerSymbol = ticker,
                    Shares = 0,
                    TradableCashBalance = portionedTradableCash
                };

                AddPosition(position);
            }

            LogCurrentPositions();
        }

        private List<string> GetTrackedStocks()
        {
            if(_appState.TrackedStocks.Count() < 5)
            {
                List<string> topGainers = _marketData.GetTopGainers().Take(5).ToList();
                _appState.SetTrackedStocks(topGainers);
            }

            return _appState.TrackedStocks;
        }

        private void LogCurrentPositions()
        {
            var stockSymbols = String.Join(",", GetWatchedPositions().Select(x => x.TickerSymbol).ToArray());
            _logger.Information("Selected stocks to track: [{TrackStocks}]", stockSymbols);
        }

        public void LogOrder(IOrder order)
        {
            _logger.Information("{OrderSide}: Order was {OrderStatus} for {Shares} shares of {TickerSymbol} at {TimeStamp}", order.OrderSide, order.OrderStatus, order.Quantity, order.Symbol, order.CreatedAt);
        }

        private void AddPosition(Position position)
        {
            _appState.AddPosition(position);
        }

        private void SetIsAnalyzed(string ticker)
        {
            var position = _appState.GetPosition(ticker);

            position.NeedsAnalyzed = false;

            _appState.SetPosition(position);
        }

        private void SetPosition(TimeSeries data)
        {
            var ticker = data.GetTickerSymbol();

            _appState.UpdateIntraDayData(data);

            var pos = _appState.GetPosition(ticker);

            _logger.Debug("Setting Position: {Ticker}, {Close}", pos.TickerSymbol, pos.IntraDayTrack.GetLatestPeriod().Close);
            LogUpdatedTechIndicators(pos);
        }

        private void SetPosition(TradeAction action)
        {
            var position = _appState.GetPosition(action.TickerSymbol);
            var orderSide = action.Action;
            var shares = action.Shares;
            var cashBalance = _exchange.GetAvailableCash() / _appState.GetCurrentPositions().Count();

            position.Shares = shares;
            position.TradableCashBalance = (double) cashBalance;

            if (orderSide == TradeActionType.Buy) position.TradableCashBalance = WithdrawFromTradableCash(shares, position);
            if (orderSide == TradeActionType.Sell) position.TradableCashBalance = DepositToTradableCash(shares, position);
            
            _appState.SetPosition(position);

            LogPositionUpdate(position, action);
        }

        private void LogUpdatedTechIndicators(Position pos)
        {
            var SMA = pos.Average._averages.Last();
            var trend = pos.Average.GetTrend();
            var open = pos.IntraDayTrack.GetLatestPeriod().Open;
            var close = pos.IntraDayTrack.GetLatestPeriod().Close;
            var trailingStop = pos.TrailingStopPrice;

            _logger.Debug("Technical Indicators: {SMA}, {Trend}, {Open}, {Close}, {Trailing Stop}", SMA, trend, open, close, trailingStop);
        }

        private void LogPositionUpdate(Position position, TradeAction action)
        {
            var close = position.IntraDayTrack.GetLatestPeriod().Close;

            _logger.Information("{Symbol} position balance has updated to \\${TradableBalance} from a {OrderSide} of {Shares} at ${Close} per a share", 
                position.TickerSymbol,
                position.TradableCashBalance,
                action.Action,
                action.Shares,
                close);

            LogUpdatedTechIndicators(position);
        }

        private double DepositToTradableCash(long shares, Position position)
        {
            var latestClose = position.IntraDayTrack.GetLatestPeriod().Close;
            var currentBalance = position.TradableCashBalance;

            return currentBalance + (latestClose * shares);
        }

        private static double WithdrawFromTradableCash(long shares, Position position)
        {
            var latestClose = position.IntraDayTrack.GetLatestPeriod().Close;
            var currentBalance = position.TradableCashBalance;

            return currentBalance - (latestClose * shares);
        }

        private void SetPositionShares(long shares, string ticker)
        {
            SetPositionShares(shares, ticker);
        }

        private void ClearPositions()
        {
            _appState.ResetPositions();
        }

        public List<Position> GetWatchedPositions()
        {
            return _appState.GetCurrentPositions();
        }

        private void SetMarketHours()
        {
            var hours = _exchange.GetMarketHours();

            _appState.MarketHours.UpdateClock(hours);
        }

        public Clock GetMarketHours()
        {
            return _appState.MarketHours;
        }

    }
}
