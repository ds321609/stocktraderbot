﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Alpaca.Markets;
using MarketDataService;

namespace StockTraderBot
{
    public class ApplicationState : IApplicationState
    {
        public Clock MarketHours { get; set; } = new Clock();
        public List<Position> Positions { get; set; } = new List<Position>();
        public List<string> TrackedStocks { get; set; } = new List<string>();

        public void AddPosition(Position position)
        {
            Positions.Add(position);   
        }

        public List<Position> GetCurrentPositions()
        {
            return Positions;
        }

        public Position GetPosition(string ticker)
        {
            return Positions.FirstOrDefault(x => x.TickerSymbol == ticker);
        }

        public void ResetPositions()
        {
            Positions.Clear();
        }

        public void SetPosition(Position position)
        {
            var indexPosition = Positions.FindIndex(x => x.TickerSymbol == position.TickerSymbol);
            Positions.RemoveAt(indexPosition);
            Positions.Add(position);
        }

        public void SetPositionShares(long shares, string ticker)
        {
            var position = Positions.Find(x => x.TickerSymbol == ticker);
            position.Shares = shares;
        } 

        public void UpdateIntraDayData(TimeSeries data)
        {
            string ticker = data.GetTickerSymbol();
            var positionToUpdate = Positions.Find(x => x.TickerSymbol == ticker);
            positionToUpdate.UpdatePosition(data);
        }

        public void SetTrackedStocks(List<string> tickers)
        {
            TrackedStocks = tickers;
        }
    }

    public class Clock : IClock
    {
        public DateTime Timestamp { get; set; }

        public bool IsOpen { get; set;  }

        public DateTime NextOpen { get; set; }

        public DateTime NextClose { get; set;  }

        public void UpdateClock(IClock clock)
        {
            Timestamp = clock.Timestamp;
            IsOpen = clock.IsOpen;
            NextOpen = clock.NextOpen;
            NextClose = clock.NextClose;
        }
    }
}
