﻿using ExchangeService.Adapters;
using System;
using System.Collections.Generic;
using Alpaca.Markets;

namespace ExchangeService
{
    public class Exchange : IExchangeAdapter
    {
        IAlpacaRestClientAdapter _client;

        public Exchange(IAlpacaRestClientAdapter client)
        {
            _client = client;
        }
        public List<OrderStatus> CancelAllOrders()
        {
            try
            {
                var response = _client.DeleteAllOrdersAsync();

                return response;

            } catch(Exception ex)
            {
                throw ex;
            }

        }
        public decimal GetAvailableCash()
        {
            try
            {
                return _client.GetAccountAsync().TradableCash;

            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public IReadOnlyList<IPosition> GetOwnedAssests()
        {
            try
            {
                return _client.ListPositionsAsync();

            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetPortfolioValue()
        {
            try
            {
                return _client.GetAccountAsync().Equity;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IOrder PurchaseStockAtLimit(string ticker, long quantity, long stop)
        {
            try
            {
                decimal stopPrice = (decimal) stop / 100;

                return _client.PostOrderAsync(ticker, quantity, OrderSide.Buy, OrderType.Stop, TimeInForce.Gtc, stopPrice: stopPrice, extendedHours: false);

            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public IOrder PurchaseStockAtMarket(string ticker, long quantity)
        {
            try
            {
                return _client.PostOrderAsync(ticker, quantity, OrderSide.Buy, OrderType.Market, TimeInForce.Gtc, extendedHours: false);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IOrder SellStockAtLimit(string ticker, long quantity, long limit)
        {
            try
            {
                decimal limitPrice = (decimal)limit / 100;

                return _client.PostOrderAsync(ticker, quantity, OrderSide.Sell, OrderType.Limit, TimeInForce.Gtc, limitPrice: limitPrice, extendedHours: false);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IOrder SellStockAtMarket(string ticker, long quantity)
        {
            try
            {
                return _client.PostOrderAsync(ticker, quantity, OrderSide.Sell, OrderType.Market, TimeInForce.Gtc, extendedHours: false);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IClock GetMarketHours()
        {
            try
            {
                IClock response = _client.GetClockAsync();
                return response;
            } catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
