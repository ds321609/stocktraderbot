﻿using Alpaca.Markets;
using ExchangeService.Adapters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeService
{
    public class AlpacaRestClient : IAlpacaRestClientAdapter
    {
        string API_URL = "https://paper-api.alpaca.markets";
        string API_SECRET = Environment.GetEnvironmentVariable("ALPACA_API_SECRET_KEY");
        string KEY_ID = Environment.GetEnvironmentVariable("ALPACA_API_KEY_ID");
        RestClient _client;

        public AlpacaRestClient()
        {
            _client = new RestClient(KEY_ID, API_SECRET, API_URL);
        }
        public AlpacaRestClient(string apiUrl, string apiKey, string apiSecret)
        {
            _client = new RestClient(apiKey, apiSecret, apiUrl);
        }

        public List<OrderStatus> DeleteAllOrdersAsync(CancellationToken cancellationToken = default)
        {
            return (List<OrderStatus>) _client.DeleteAllOrdersAsync().Result;
        }

        public Task<IReadOnlyList<IPositionActionStatus>> DeleteAllPositionsAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteOrderAsync(Guid orderId, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeletePositionAsync(string symbol, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IAccount GetAccountAsync(CancellationToken cancellationToken = default)
        {
            var response = _client.GetAccountAsync().Result;

            return response;
        }

        public Task<IAccountConfiguration> GetAccountConfigurationAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IAsset> GetAssetAsync(string symbol, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyDictionary<string, IEnumerable<IAgg>>> GetBarSetAsync(IEnumerable<string> symbols, TimeFrame timeFrame, int? limit = 100, bool areTimesInclusive = true, DateTime? timeFrom = null, DateTime? timeInto = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public IClock GetClockAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                return _client.GetClockAsync().Result;
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public Task<IReadOnlyDictionary<long, string>> GetConditionMapAsync(TickType tickType = TickType.Trades, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<ILastQuote> GetLastQuoteAsync(string symbol, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<ILastTrade> GetLastTradeAsync(string symbol, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IOrder> GetOrderAsync(string clientOrderId, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IOrder> GetOrderAsync(Guid orderId, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public IPosition GetPositionAsync(string symbol, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyDictionary<string, string>> GetSymbolTypeMapAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<IAsset>> ListAccountActivitiesAsync(IEnumerable<AccountActivityType> activityTypes = null, DateTime? date = null, DateTime? until = null, DateTime? after = null, SortDirection? direction = null, long? pageSize = null, string pageToken = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<IAsset>> ListAssetsAsync(AssetStatus? assetStatus = null, AssetClass? assetClass = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<ICalendar>> ListCalendarAsync(DateTime? startDateInclusive = null, DateTime? endDateInclusive = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IAggHistoricalItems<IAgg>> ListDayAggregatesAsync(string symbol, DateTime? dateFromInclusive = null, DateTime? dateIntoInclusive = null, int? limit = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IHistoricalItems<IAgg>> ListDayAggregatesAsync(string symbol, int multiplier, DateTime dateFromInclusive, DateTime dateToInclusive, bool unadjusted = false, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<IExchange>> ListExchangesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IDayHistoricalItems<IHistoricalQuote>> ListHistoricalQuotesAsync(string symbol, DateTime date, long? offset = null, int? limit = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IDayHistoricalItems<IHistoricalTrade>> ListHistoricalTradesAsync(string symbol, DateTime date, long? offset = null, int? limit = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IAggHistoricalItems<IAgg>> ListMinuteAggregatesAsync(string symbol, DateTime? dateFromInclusive = null, DateTime? dateIntoInclusive = null, int? limit = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IHistoricalItems<IAgg>> ListMinuteAggregatesAsync(string symbol, int multiplier, DateTime dateFromInclusive, DateTime dateToInclusive, bool unadjusted = false, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<IOrder>> ListOrdersAsync(OrderStatusFilter? orderStatusFilter = null, SortDirection? orderListSorting = null, DateTime? untilDateTimeExclusive = null, DateTime? afterDateTimeExclusive = null, long? limitOrderNumber = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<IPosition> ListPositionsAsync(CancellationToken cancellationToken = default)
        {
            return _client.ListPositionsAsync().Result;
        }

        public Task<IAccountConfiguration> PatchAccountConfigurationAsync(IAccountConfiguration accountConfiguration, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IOrder> PatchOrderAsync(Guid orderId, long? quantity = null, TimeInForce? duration = null, decimal? limitPrice = null, decimal? stopPrice = null, string clientOrderId = null, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public IOrder PostOrderAsync(string symbol, long quantity, OrderSide side, OrderType type, TimeInForce duration, decimal? limitPrice = null, decimal? stopPrice = null, string clientOrderId = null, bool? extendedHours = null, CancellationToken cancellationToken = default)
        {
            return _client.PostOrderAsync(symbol, quantity, side, type, duration, limitPrice, stopPrice, clientOrderId, extendedHours, cancellationToken).Result;
        }
    }
}
