﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Alpaca.Markets;

namespace ExchangeService
{
    public class Account : IAccount
    {
        public Account(){}

        public Account(
            string accountNumber = "",
            decimal portfolioValue = 0,
            bool isDayPatternTrader = false,
            bool isTradingBlocked = false,
            bool isTransfersBlocked = false,
            bool tradeSuspendedByUser = false,
            bool shortingEnabled = false,
            long multiplier = 1,
            decimal buyingPower = 0,
            decimal dayTradingBuyingPower = 0,
            decimal regulationBuyingPower = 0,
            decimal longMarketValue = 0,
            decimal shortMarketValue = 0,
            decimal equity = 0,
            decimal lastEquity = 0,
            decimal initialMargin = 0,
            decimal maintenanceMargin = 0,
            decimal lastMaintenanceMargin = 0,
            long dayTradeCount = 0,
            decimal sma = 0,
            bool isAccountBlocked = false,
            Guid accountId = new Guid(),
            AccountStatus status = AccountStatus.Active,
            string currency = "USD",
            decimal tradableCash = 0,
            decimal withdrawableCash = 0,
            DateTime createdAt = new DateTime()
        )
        {
            AccountNumber = accountNumber;
            PortfolioValue = portfolioValue;
            IsDayPatternTrader = isDayPatternTrader;
            IsTradingBlocked = isTradingBlocked;
            IsTransfersBlocked = isTransfersBlocked;
            TradeSuspendedByUser = tradeSuspendedByUser;
            ShortingEnabled = shortingEnabled;
            Multiplier = multiplier;
            BuyingPower = buyingPower;
            DayTradingBuyingPower = dayTradingBuyingPower;
            RegulationBuyingPower = regulationBuyingPower;
            LongMarketValue = longMarketValue;
            ShortMarketValue = shortMarketValue;
            Equity = equity;
            LastEquity = lastEquity;
            InitialMargin = initialMargin;
            MaintenanceMargin = maintenanceMargin;
            LastMaintenanceMargin = lastMaintenanceMargin;
            DayTradeCount = dayTradeCount;
            Sma = sma;
            IsAccountBlocked = isAccountBlocked;
            AccountId = accountId;
            Status = status;
            Currency = currency;
            TradableCash = tradableCash;
            WithdrawableCash = withdrawableCash;
            CreatedAt = createdAt;

        }


        public string AccountNumber { get; }

        public decimal PortfolioValue { get; }

        public bool IsDayPatternTrader { get; }

        public bool IsTradingBlocked { get; }

        public bool IsTransfersBlocked { get; }

        public bool TradeSuspendedByUser { get; }

        public bool ShortingEnabled { get; }

        public long Multiplier { get; }

        public decimal BuyingPower { get; }

        public decimal DayTradingBuyingPower { get; }

        public decimal RegulationBuyingPower { get; }

        public decimal LongMarketValue { get; }

        public decimal ShortMarketValue { get; }

        public decimal Equity { get; }

        public decimal LastEquity { get; }

        public decimal InitialMargin { get; }

        public decimal MaintenanceMargin { get; }

        public decimal LastMaintenanceMargin { get; }

        public long DayTradeCount { get; }

        public decimal Sma { get; }

        public bool IsAccountBlocked { get; }

        public Guid AccountId { get; }

        public AccountStatus Status { get; }

        public string Currency { get; }

        public decimal TradableCash { get; }

        public decimal WithdrawableCash { get; }

        public DateTime CreatedAt { get; }
    }
}
