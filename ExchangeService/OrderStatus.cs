﻿using System;
using System.Collections.Generic;
using System.Text;
using Alpaca.Markets;

namespace ExchangeService
{
    public class OrderStatus : IOrderActionStatus
    {
        public Guid OrderId { get; set; }
        public bool IsSuccess { get; set; }
    }
}
