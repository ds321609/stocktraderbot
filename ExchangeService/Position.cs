﻿using Alpaca.Markets;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeService
{
    public class Position : IPosition
    {
        public Guid AccountId { get; }

        public Guid AssetId { get; }

        public string Symbol { get; }

        public Alpaca.Markets.Exchange Exchange { get; }

        public AssetClass AssetClass { get; }

        public decimal AverageEntryPrice { get; }

        public int Quantity { get; }

        public PositionSide Side { get; }

        public decimal MarketValue { get; }

        public decimal CostBasis { get; }

        public decimal UnrealizedProfitLoss { get; }

        public decimal UnrealizedProfitLossPercent { get; }

        public decimal IntradayUnrealizedProfitLoss { get; }

        public decimal IntradayUnrealizedProfitLossPercent { get; }

        public decimal AssetCurrentPrice { get; }

        public decimal AssetLastPrice { get; }

        public decimal AssetChangePercent { get; }
    }
}
