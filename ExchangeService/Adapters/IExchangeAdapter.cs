﻿using System;
using System.Collections.Generic;
using System.Text;
using Alpaca.Markets;

namespace ExchangeService.Adapters
{
    public interface IExchangeAdapter
    {
        decimal GetAvailableCash();
        IReadOnlyList<IPosition> GetOwnedAssests();
        decimal GetPortfolioValue();
        IOrder PurchaseStockAtLimit(string ticker, long quantity, long stop);
        IOrder PurchaseStockAtMarket(string ticker, long quantity);
        IOrder SellStockAtLimit(string ticker, long quantity, long limit);
        IOrder SellStockAtMarket(string ticker, long quantity);
        List<OrderStatus> CancelAllOrders();
        IClock GetMarketHours();
    }
}
