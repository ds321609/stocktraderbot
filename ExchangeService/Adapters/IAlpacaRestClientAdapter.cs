﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Alpaca.Markets;
using Microsoft.Extensions.Configuration;

namespace ExchangeService.Adapters
{
    public interface IAlpacaRestClientAdapter
    {     
         List<OrderStatus> DeleteAllOrdersAsync(CancellationToken cancellationToken = default);
         Task<IReadOnlyList<IPositionActionStatus>> DeleteAllPositionsAsync(CancellationToken cancellationToken = default);
         Task<bool> DeleteOrderAsync(Guid orderId, CancellationToken cancellationToken = default);
         Task<bool> DeletePositionAsync(string symbol, CancellationToken cancellationToken = default);
         void Dispose();
         IAccount GetAccountAsync(CancellationToken cancellationToken = default);
         Task<IAccountConfiguration> GetAccountConfigurationAsync(CancellationToken cancellationToken = default);
         Task<IAsset> GetAssetAsync(string symbol, CancellationToken cancellationToken = default);
         Task<IReadOnlyDictionary<string, IEnumerable<IAgg>>> GetBarSetAsync(IEnumerable<string> symbols, TimeFrame timeFrame, int? limit = 100, bool areTimesInclusive = true, DateTime? timeFrom = null, DateTime? timeInto = null, CancellationToken cancellationToken = default);
         IClock GetClockAsync(CancellationToken cancellationToken = default);
         Task<IReadOnlyDictionary<long, string>> GetConditionMapAsync(TickType tickType = TickType.Trades, CancellationToken cancellationToken = default);
         Task<ILastQuote> GetLastQuoteAsync(string symbol, CancellationToken cancellationToken = default);
         Task<ILastTrade> GetLastTradeAsync(string symbol, CancellationToken cancellationToken = default);
         Task<IOrder> GetOrderAsync(string clientOrderId, CancellationToken cancellationToken = default);
         Task<IOrder> GetOrderAsync(Guid orderId, CancellationToken cancellationToken = default);
         IPosition GetPositionAsync(string symbol, CancellationToken cancellationToken = default);
         Task<IReadOnlyDictionary<string, string>> GetSymbolTypeMapAsync(CancellationToken cancellationToken = default);
         Task<IReadOnlyList<IAsset>> ListAccountActivitiesAsync(IEnumerable<AccountActivityType> activityTypes = null, DateTime? date = null, DateTime? until = null, DateTime? after = null, SortDirection? direction = null, long? pageSize = null, string pageToken = null, CancellationToken cancellationToken = default);
         Task<IReadOnlyList<IAsset>> ListAssetsAsync(AssetStatus? assetStatus = null, AssetClass? assetClass = null, CancellationToken cancellationToken = default);
         Task<IReadOnlyList<ICalendar>> ListCalendarAsync(DateTime? startDateInclusive = null, DateTime? endDateInclusive = null, CancellationToken cancellationToken = default);
         Task<IAggHistoricalItems<IAgg>> ListDayAggregatesAsync(string symbol, DateTime? dateFromInclusive = null, DateTime? dateIntoInclusive = null, int? limit = null, CancellationToken cancellationToken = default);
         Task<IHistoricalItems<IAgg>> ListDayAggregatesAsync(string symbol, int multiplier, DateTime dateFromInclusive, DateTime dateToInclusive, bool unadjusted = false, CancellationToken cancellationToken = default);
         Task<IReadOnlyList<IExchange>> ListExchangesAsync(CancellationToken cancellationToken = default);
         Task<IDayHistoricalItems<IHistoricalQuote>> ListHistoricalQuotesAsync(string symbol, DateTime date, long? offset = null, int? limit = null, CancellationToken cancellationToken = default);
         Task<IDayHistoricalItems<IHistoricalTrade>> ListHistoricalTradesAsync(string symbol, DateTime date, long? offset = null, int? limit = null, CancellationToken cancellationToken = default);
         Task<IAggHistoricalItems<IAgg>> ListMinuteAggregatesAsync(string symbol, DateTime? dateFromInclusive = null, DateTime? dateIntoInclusive = null, int? limit = null, CancellationToken cancellationToken = default);
         Task<IHistoricalItems<IAgg>> ListMinuteAggregatesAsync(string symbol, int multiplier, DateTime dateFromInclusive, DateTime dateToInclusive, bool unadjusted = false, CancellationToken cancellationToken = default);
         Task<IReadOnlyList<IOrder>> ListOrdersAsync(OrderStatusFilter? orderStatusFilter = null, SortDirection? orderListSorting = null, DateTime? untilDateTimeExclusive = null, DateTime? afterDateTimeExclusive = null, long? limitOrderNumber = null, CancellationToken cancellationToken = default);
         IReadOnlyList<IPosition> ListPositionsAsync(CancellationToken cancellationToken = default);
         Task<IAccountConfiguration> PatchAccountConfigurationAsync(IAccountConfiguration accountConfiguration, CancellationToken cancellationToken = default);
         Task<IOrder> PatchOrderAsync(Guid orderId, long? quantity = null, TimeInForce? duration = null, decimal? limitPrice = null, decimal? stopPrice = null, string clientOrderId = null, CancellationToken cancellationToken = default);
         IOrder PostOrderAsync(string symbol, long quantity, OrderSide side, OrderType type, TimeInForce duration, decimal? limitPrice = null, decimal? stopPrice = null, string clientOrderId = null, bool? extendedHours = null, CancellationToken cancellationToken = default);

    }
}
