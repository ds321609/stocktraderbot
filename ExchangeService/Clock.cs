﻿using Alpaca.Markets;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeService
{
    public class Clock : IClock
    {
        public DateTime Timestamp { get; set; }

        public bool IsOpen { get; set; }

        public DateTime NextOpen { get; set; }

        public DateTime NextClose { get; set; }
    }
}
