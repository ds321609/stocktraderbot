﻿using Xunit;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using ExchangeService.Adapters;
using ExchangeService;
using MarketDataService;
using Moq;
using Serilog;
using System.Net;

namespace SUT
{
    public class IntegrationTests
    {
        [Fact()]
        public void SessionHandlerAtOpenTest()
        {
            var mockMarketData = MockMarketData();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();

            mockAppState.MarketHours = new StockTraderBot.Clock();
            mockAppState.MarketHours.NextOpen = DateTime.Parse("01/16/2020 9:30:00");
            mockAppState.MarketHours.NextClose = DateTime.Parse("01/16/2020 16:00:00");

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            sut.SessionHandler(DateTime.Parse("01/16/2020 10:00:00"));

            var actualPositions = sut.GetWatchedPositions();

            Assert.Equal(5, actualPositions.Count);
            Assert.All(actualPositions, x => Assert.True(x.IntraDayTrack.GetPeriods().Count > 0));
        }

        [Fact()]
        public void SessionHandlerAfterOpenTest()
        {
            var mockMarketData = MockMarketData();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();

            mockAppState.MarketHours = new StockTraderBot.Clock();
            mockAppState.MarketHours.NextOpen = DateTime.Parse("01/16/2020 9:30:00");
            mockAppState.MarketHours.NextClose = DateTime.Parse("01/16/2020 16:00:00");

            mockAppState.Positions = new List<Position>() {
                new Position()
                {
                    TickerSymbol = "DNLI"
                },
                new Position()
                {
                    TickerSymbol = "FTSV"
                },
                new Position()
                {
                    TickerSymbol = "ICHBF"
                },
                new Position()
                {
                    TickerSymbol = "LSCC"
                },
                new Position()
                {
                    TickerSymbol = "PRNB"
                }

            };

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            sut.SessionHandler(DateTime.Parse("01/16/2020 10:00:00"));

            var actualPositions = sut.GetWatchedPositions();

            Assert.Equal(5, actualPositions.Count);
            Assert.All(actualPositions, x => Assert.True(x.IntraDayTrack.GetPeriods().Count > 0));
        }

        private Mock<IMarketDataAdapter> MockMarketData()
        {
            var mockData = new Mock<IMarketDataAdapter>();

            mockData.SetupSequence(x => x.GetStockIntraDay(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\DNLI-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\FTSV-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\ICHBF-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\LSCC-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\PRNB-test-data.json")));

            mockData.Setup(x => x.GetTopGainers()).Returns(new List<string>() { "DNLI", "FTSV", "ICHBF", "LSCC", "PRNB" });

            return mockData;
        }
    }
}