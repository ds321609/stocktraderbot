﻿using Xunit;
using MarketDataService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using Moq;
using Moq.Protected;
using DeepEqual.Syntax;
using System.IO;
using System.Linq;

namespace SUT
{
    public class MarketDataTests
    {

        [Fact()]
        public void GetIntraDayPeriodsTest()
        {
            string json = Helpers.ExtractJsonFromFile(@"Test Data\test-intraday-full.json");
            HttpClient httpClient = Helpers.MockHttpHandler(HttpStatusCode.OK,
                json);

            var sut = new MarketData(httpClient, "demo");
            var expectedMetadata = new MetaData()
            {
                Information = "Intraday (1min) open, high, low, close prices and volume",
                Symbol = "CURLF",
                LastRefreshed = DateTime.Parse("2019-12-27 16:00:00"),
                Interval = "1min",
                OutputSize = "Full size",
                Zone = "US/Eastern"
            };

            var actualTimeSeries = sut.GetStockIntraDay("CURLF", true).GetPeriods();
            var expectedTimestamps = new List<DateTime>
            {
                DateTime.Parse("2019-12-27 16:00:00"),
                DateTime.Parse("2019-12-27 15:59:00"),
                DateTime.Parse("2019-12-26 09:43:00"),
                DateTime.Parse("2019-12-23 10:34:00"),

            };

            var doesReturnAllExpected = actualTimeSeries.Select(x => x.Timestamp).Intersect(expectedTimestamps).ToArray();
            var getAllIntradayPeriods = sut.GetStockIntraDay("CURLF", true).GetIntraDayToday();
            var expectedDate = DateTime.Now.Date;

            Assert.True(doesReturnAllExpected.Length == 4);
            Assert.True(getAllIntradayPeriods.TrueForAll(x => x.Timestamp.Date == expectedDate));
        }

        [Fact()]
        public void GetGlobalQuoteTest()
        {
            HttpClient httpClient = Helpers.MockHttpHandler(HttpStatusCode.OK,
                "{\n    \"Global Quote\": {\n        \"01. symbol\": \"MSFT\",\n        \"02. open\": \"158.1200\",\n        \"03. high\": \"158.1200\",\n        \"04. low\": \"157.5400\",\n        \"05. price\": \"157.7350\",\n        \"06. volume\": \"1209812\",\n        \"07. latest trading day\": \"2019-12-23\",\n        \"08. previous close\": \"157.4100\",\n        \"09. change\": \"0.3250\",\n        \"10. change percent\": \"0.2065%\"\n    }\n}");

            var sut = new MarketData(httpClient, "demo");

            var expectedResult = new GlobalQuote
            {
                TickerSymbol = "MSFT",
                Open = 158.120,
                High = 158.120,
                Low = 157.540,
                CurrentPrice = 157.735,
                Volume = 1209812,
                Close = 157.41,
                Change = 0.3250
            };
            var result = sut.GetGlobalQuote("MSFT");

            Assert.NotNull(result);
            expectedResult.ShouldDeepEqual(result);

            var expectedUri = new Uri("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=MSFT&apikey=demo");

        }

        [Fact()]
        public void GetGlobalQuoteFailsTest()
        {
            HttpClient httpClient = Helpers.MockHttpHandler(HttpStatusCode.NotFound,
                "{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https://www.alphavantage.co/documentation/) for GLOBAL_QUOTE.\"}");

            var sut = new MarketData(httpClient, "demo");

            Assert.Throws<Exception>(() => sut.GetGlobalQuote("BAD"));
        }

        [Fact()]
        public void GetTopGainersTest()
        {
            string json = Helpers.ExtractJsonFromFile(@"Test Data\test-stock-data.json");

            HttpClient httpClient = Helpers.MockHttpHandler(HttpStatusCode.OK,
                json);

            var sut = new MarketData(httpClient, "demo");

            var quotes = sut.GetTopGainers();
            var expectedQuotes = new string[5] { "WHGRF", "CBB-PB", "TXG", "APA", "LGF-A" };

            Assert.All(quotes, x => expectedQuotes.Contains(x));
        }

        [Fact()]
        public void GetTopGainersFailedTest()
        {
            HttpClient httpClient = Helpers.MockHttpHandler(HttpStatusCode.BadRequest, "");

            var sut = new MarketData(httpClient, "demo");

            Assert.Throws<Exception>(() => sut.GetTopGainers());
        }

        //private static string Helpers.ExtractJsonFromFile(string filePath)
        //{
        //    try
        //    {
        //        string result = "";
        //        using (StreamReader sr = new StreamReader(filePath))
        //        {
        //            string line;
        //            while ((line = sr.ReadLine()) != null)
        //            {
        //                result += line;
        //            }
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}