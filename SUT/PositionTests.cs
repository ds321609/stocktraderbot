﻿using Xunit;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MarketDataService;
using StockTraderBot.Technical_Indicators;

namespace SUT
{
    public class PositionTests
    {
        [Theory()]
        [MemberData(nameof(GetSamplePositions))]
        public void UpdatePositionTest(TimeSeries timeSeries, double sma, double stop)
        {
            var position = new Position();
            List<Period> initalPeriod = new List<Period>() {
                new Period()
                {
                    Open = 5.6801,
                    High = 5.7889,
                    Low = 5.4243,
                    Close = 5.6447,
                    Volume = 2145
                }
            };
            position.Average = new SimpleMovingAverage(new double[1] { 1.0 });
            position.IntraDayTrack = new TimeSeries(initalPeriod);
            position.UpdatePosition(timeSeries);

            Assert.Equal(sma, Math.Round(position.Average._averages.Last(), digits: 2));
            Assert.Equal(stop, Math.Round(position.TrailingStopPrice, digits: 2));
        }

        [Fact()]
        public void SetSMAFromTimeSeriesDataTest()
        {
            var timeSeries = new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\ENPH-test-data.json"));
            var position = new Position()
            {
                IntraDayTrack = timeSeries
            };

            position.Average.SetSMAFromTimeSeriesData(5, timeSeries);

            var expected = new double[5] { 56.5746, 56.641, 56.70318, 56.81418, 56.9345 };

            Assert.Equal(expected, position.Average._averages);
        }

        public static IEnumerable<object[]> GetSamplePositions()
        {
            List<Period> initalPeriod = new List<Period>() {
                new Period()
                {
                    Open = 5.6801,
                    High = 5.7889,
                    Low = 5.4243,
                    Close = 5.6447,
                    Volume = 2145
                }
            };

            List<Period> dataPoints1 = new List<Period>()
            {
                new Period()
                {
                    Open = 5.6801,
                    High = 5.7889,
                    Low = 5.4243,
                    Close = 5.6447,
                    Volume = 2145
                },
                new Period()
                {
                    Open = 5.4940,
                    High = 5.4940,
                    Low = 5.4940,
                    Close = 5.4940,
                    Volume = 900
                },
                new Period()
                {
                    Open = 5.4901,
                    High = 5.5189,
                    Low = 5.4843,
                    Close = 5.5147,
                    Volume = 2145
                }
            };

            List<Period> dataPoints2 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.0736,
                    High = 4.1000,
                    Low = 4.0400,
                    Close = 4.0400,
                    Volume = 1300000
                },
                new Period()
                {
                    Open = 3.93,
                    High = 3.96,
                    Low = 3.84,
                    Close = 3.8627,
                    Volume = 1330000
                },
                new Period()
                {
                    Open = 3.94,
                    High = 4.00,
                    Low = 3.940,
                    Close = 3.9799,
                    Volume = 1340000
                }
            };

            List<Period> dataPoints3 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.4812,
                    High = 4.68,
                    Low = 4.480,
                    Close = 4.6793,
                    Volume = 3810000
                },
                new Period()
                {
                    Open = 4.0200,
                    High = 4.0300,
                    Low = 3.9700,
                    Close = 3.9800,
                    Volume = 4660000
                },
                new Period()
                {
                    Open = 4.82,
                    High = 4.85,
                    Low = 4.5700,
                    Close = 4.6550,
                    Volume = 3590000
                }
            };

            var mockTime = DateTime.Parse("01/16/2020 11:11:00");

            Position position = new Position()
            {
                TickerSymbol = "TEST",
                Shares = 0,
                TradableCashBalance = 3000000,
            };

            yield return new object[] { new TimeSeries(dataPoints1, mockTime), 5.55, 4.96 };

            yield return new object[] { new TimeSeries(dataPoints2, mockTime), 3.96, 3.58 };

            yield return new object[] { new TimeSeries(dataPoints3, mockTime), 4.44, 4.19 };
        }
    }
}