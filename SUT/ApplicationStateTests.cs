﻿using Xunit;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Moq;

namespace SUT
{
    public class ApplicationStateTests
    {
        [Fact()]
        public void AddPositionTest()
        {
            var sut = new ApplicationState();

            var pos = new Position() { TickerSymbol = "TEST" };

            sut.AddPosition(pos);

            Assert.Single(sut.Positions);

        }

        [Fact()]
        public void GetCurrentPositionsTest()
        {
            var sut = new ApplicationState();
            sut.Positions = new List<Position>() { new Position() { TickerSymbol = "TEST" } };

            Assert.Single(sut.GetCurrentPositions());
        }

        [Fact()]
        public void GetPositionTest()
        {
            var sut = new ApplicationState();

            sut.Positions.Add(new Position() { TickerSymbol = "TEST" });
            sut.Positions.Add(new Position() { TickerSymbol = "FOOBAR" });

            Assert.Equal("FOOBAR", sut.GetPosition("FOOBAR").TickerSymbol);
           
        }

        [Fact()]
        public void ResetPositionsTest()
        {
            var sut = new ApplicationState();

            sut.Positions.Add(new Position() { TickerSymbol = "TEST" });
            sut.Positions.Add(new Position() { TickerSymbol = "FOOBAR" });

            sut.ResetPositions();
            Assert.Empty(sut.Positions);
        }

        [Fact()]
        public void SetPositionTest()
        {
            var sut = new ApplicationState();

            sut.Positions.Add(new Position() { TickerSymbol = "TEST", Shares = 100 });
            sut.Positions.Add(new Position() { TickerSymbol = "FOOBAR", Shares = 1000 });

            sut.SetPosition(new Position() { TickerSymbol = "FOOBAR", Shares = 5000 });

            var actual = sut.Positions.Find(x => x.TickerSymbol == "FOOBAR");
            Assert.Equal(5000, actual.Shares);
        }
    }
}