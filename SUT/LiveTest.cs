﻿using Alpaca.Markets;
using ExchangeService.Adapters;
using MarketDataService;
using Moq;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using Serilog;
using Xunit;

namespace SUT
{
    public class LiveTest
    {
        [Fact()]
        public void MarketUpdateTest()
        {
            var marketData = new MarketData();
            var mockExchange = new Mock<IExchangeAdapter>();
            var appState = new ApplicationState();
            var logger = new Mock<ILogger>();

            appState.MarketHours = new Clock();
            appState.MarketHours.NextOpen = DateTime.Parse("01/23/2020 9:30:00");
            appState.MarketHours.NextClose = DateTime.Parse("01/23/2020 16:00:00");
            appState.TrackedStocks = new List<string>() { "FORU", "USNZY", "SPCE", "LX", "SFBS" };

            var sut = new Application(mockExchange.Object, marketData, appState, logger.Object);

            sut.SessionHandler(DateTime.Parse("01/23/2020 10:30:00"));
            var positions = sut.GetWatchedPositions();

            Assert.All(positions, x => Assert.True(AreDuplicatesInPositions(x.TickerSymbol, positions)));
        }

        private bool AreDuplicatesInPositions(string ticker, List<Position> positions)
        {
            var count = positions.FindAll(x => x.TickerSymbol == ticker).Count;

            return count == 1;
        }
    }
}
