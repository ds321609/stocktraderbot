﻿using Xunit;
using ExchangeService;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using Moq;
using Alpaca.Markets;
using ExchangeService.Adapters;

namespace SUT
{
    public class ExchangeTests
    {
        [Fact()]
        public void CancelAllOrdersTest()
        {
            var expectedResponse = new List<ExchangeService.OrderStatus>()
            {
                new ExchangeService.OrderStatus() { OrderId = new Guid(), IsSuccess = true},
            };

            expectedResponse.AsReadOnly();

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.DeleteAllOrdersAsync(new CancellationToken())).Returns(expectedResponse);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);

            Assert.Equal(expectedResponse, sut.CancelAllOrders());

        }

        [Fact()]
        public void GetAvailableCashTest()
        {
            var expectedDollarAmount = new Mock<IAccount>();

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            expectedDollarAmount.SetupGet(x => x.TradableCash).Returns(200012);
            mockRestClient.Setup(x => x.GetAccountAsync(new CancellationToken())).Returns(expectedDollarAmount.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);

            Assert.Equal(200012, sut.GetAvailableCash());
        }

        [Fact()]
        public void GetOwnedAssestsTest()
        {
            var expectedAsset = new Mock<IPosition>();

            expectedAsset.Setup(x => x.Symbol).Returns("TEST");

            var expectedListOfAssets = new Mock<IReadOnlyList<IPosition>>();

            expectedListOfAssets.SetupGet(x => x[It.IsAny<int>()]).Returns(expectedAsset.Object);

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.ListPositionsAsync(new CancellationToken())).Returns(expectedListOfAssets.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);

            Assert.Equal("TEST", sut.GetOwnedAssests()[0].Symbol);
        }

        [Fact()]
        public void GetPortfolioValueTest()
        {

            var expectedDollarAmount = new Mock<IAccount>();

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            expectedDollarAmount.SetupGet(x => x.Equity).Returns(100000);
            mockRestClient.Setup(x => x.GetAccountAsync(new CancellationToken())).Returns(expectedDollarAmount.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);

            Assert.Equal(100000, sut.GetPortfolioValue());
        }

        [Fact()]
        public void PurchaseStockAtLimitTest()
        {
            var mockOrder = new Mock<IOrder>();

            mockOrder.Setup(x => x.OrderSide).Returns(() => OrderSide.Buy);
            mockOrder.Setup(x => x.StopPrice).Returns((decimal) 10.52);
            mockOrder.Setup(x => x.Symbol).Returns("TEST");
            mockOrder.Setup(x => x.TimeInForce).Returns(() => TimeInForce.Gtc);

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.PostOrderAsync(
                "TEST",
                1000,
                OrderSide.Buy,
                OrderType.Stop,
                TimeInForce.Gtc,
                null,
                (decimal)10.52,
                null,
                false,
                new CancellationToken())).Returns(mockOrder.Object);
            
            var sut = new ExchangeService.Exchange(mockRestClient.Object);
            var actualOrder = sut.PurchaseStockAtLimit("TEST", 1000, 1052);

            Assert.Equal("TEST", actualOrder.Symbol);
            Assert.Equal((decimal) 10.52, actualOrder.StopPrice);
            Assert.Equal(OrderSide.Buy, actualOrder.OrderSide);
        }

        [Fact()]
        public void PurchaseStockAtMarketTest()
        {
            var mockOrder = new Mock<IOrder>();

            mockOrder.Setup(x => x.OrderSide).Returns(() => OrderSide.Buy);
            mockOrder.Setup(x => x.Symbol).Returns("TEST");
            mockOrder.Setup(x => x.TimeInForce).Returns(() => TimeInForce.Gtc);

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.PostOrderAsync(
                "TEST",
                1000,
                OrderSide.Buy,
                OrderType.Market,
                TimeInForce.Gtc,
                null,
                null,
                null,
                false,
                new CancellationToken())).Returns(mockOrder.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);
            var actualOrder = sut.PurchaseStockAtMarket("TEST", 1000);

            Assert.Equal("TEST", actualOrder.Symbol);
            Assert.Equal(OrderSide.Buy, actualOrder.OrderSide);
        }

        [Fact()]
        public void SellStockAtLimitTest()
        {
            var mockOrder = new Mock<IOrder>();

            mockOrder.Setup(x => x.OrderSide).Returns(() => OrderSide.Sell);
            mockOrder.Setup(x => x.OrderType).Returns(() => OrderType.Limit);
            mockOrder.Setup(x => x.Symbol).Returns("TEST");
            mockOrder.Setup(x => x.LimitPrice).Returns((decimal) 21.93);
            mockOrder.Setup(x => x.TimeInForce).Returns(() => TimeInForce.Gtc);

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.PostOrderAsync(
                "TEST",
                1000,
                OrderSide.Sell,
                OrderType.Limit,
                TimeInForce.Gtc,
                (decimal) 21.93,
                null,
                null,
                false,
                new CancellationToken())).Returns(mockOrder.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);
            var actualOrder = sut.SellStockAtLimit("TEST", 1000, 2193);

            Assert.Equal("TEST", actualOrder.Symbol);
            Assert.Equal(OrderSide.Sell, actualOrder.OrderSide);
            Assert.Equal(OrderType.Limit, actualOrder.OrderType);
            Assert.Equal((decimal) 21.93, actualOrder.LimitPrice);
        }

        [Fact()]
        public void SellStockAtMarketTest()
        {
            var mockOrder = new Mock<IOrder>();

            mockOrder.Setup(x => x.OrderSide).Returns(() => OrderSide.Sell);
            mockOrder.Setup(x => x.OrderType).Returns(() => OrderType.Market);
            mockOrder.Setup(x => x.Symbol).Returns("TEST");
            mockOrder.Setup(x => x.TimeInForce).Returns(() => TimeInForce.Gtc);

            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockRestClient.Setup(x => x.PostOrderAsync(
                "TEST",
                1000,
                OrderSide.Sell,
                OrderType.Market,
                TimeInForce.Gtc,
                null,
                null,
                null,
                false,
                new CancellationToken())).Returns(mockOrder.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);
            var actualOrder = sut.SellStockAtMarket("TEST", 1000);

            Assert.Equal("TEST", actualOrder.Symbol);
            Assert.Equal(OrderSide.Sell, actualOrder.OrderSide);
            Assert.Equal(OrderType.Market, actualOrder.OrderType);
        }

        [Fact()]
        public void GetClockTest()
        {
            DateTime currentDate = DateTime.UtcNow;
            var mockClock = new Mock<IClock>();
            var mockRestClient = new Mock<IAlpacaRestClientAdapter>();

            mockClock.Setup(x => x.Timestamp).Returns(currentDate);
            mockClock.Setup(x => x.IsOpen).Returns(true);

            mockRestClient.Setup(x => x.GetClockAsync(new CancellationToken()))
                    .Returns(mockClock.Object);

            var sut = new ExchangeService.Exchange(mockRestClient.Object);
            var actualClock = sut.GetMarketHours();

            Assert.Equal(currentDate, actualClock.Timestamp);
            Assert.True(actualClock.IsOpen);
        }
    }
}