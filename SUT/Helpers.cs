﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using MarketDataService.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MarketDataService;

namespace SUT
{
    public static class Helpers
    {
        public static HttpClient MockHttpHandler(HttpStatusCode statusCode, string content)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = statusCode,
                    Content = new StringContent(content),
                })
                .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            return httpClient;
        }

        public static string ExtractJsonFromFile(string filePath)
        {
            try
            {
                string result = "";

                using (StreamReader sr = new StreamReader(filePath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        result += line;
                    }
                }

                result = DeseralizeWithRules(result);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static List<Period> ExtractFromJson()
        {
            throw new NotImplementedException();
        }

        private static string DeseralizeWithRules(string json)
        {
            JsonSerializer serializer = new JsonSerializer();
            var jObj = serializer.Deserialize(new JReader(new StringReader(json))) as JObject;

            return jObj.ToString(Newtonsoft.Json.Formatting.None);


        }
    }
}
