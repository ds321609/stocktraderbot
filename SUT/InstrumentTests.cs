﻿using Xunit;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using ExchangeService.Adapters;
using Moq;
using Serilog;
using System.Net;
using MarketDataService;
using System.Net.Http;
using StockTraderBot.Models;
using StockTraderBot.Technical_Indicators;

namespace SUT
{
    public class InstrumentTests
    {
        [Theory]
        [MemberData(nameof(GetSamplePatternPositions))]
        public void AnalyzePositionBuyTest(Position position)
        {
            TradeAction action = Instrument.AnalyzePosition(position);

            Assert.Equal(TradeActionType.Buy, action.Action);
        }

        public static IEnumerable<object[]> GetSamplePatternPositions()
        {
            List<Period> dataPoints1 = new List<Period>()
            {
                new Period()
                {
                    Open = 0.0039,
                    High = 0.0039,
                    Low = 0.0036,
                    Close = 0.0039,
                    Volume = 810000
                },
                new Period()
                {
                    Open = 0.0035,
                    High = 0.0035,
                    Low = 0.0033,
                    Close = 0.0033,
                    Volume = 412830
                },
                new Period()
                {
                    Open = 0.00331,
                    High = 0.0039,
                    Low = 0.0033,
                    Close = 0.0040,
                    Volume = 412830
                }
            };

            List<Period> dataPoints2 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.41,
                    High = 4.41,
                    Low = 4.38,
                    Close = 4.39,
                    Volume = 378260
                },
                new Period()
                {
                    Open = 4.3450,
                    High = 4.36,
                    Low = 4.34,
                    Close = 4.315,
                    Volume = 8666260
                },
                new Period()
                {
                    Open = 4.3151,
                    High = 4.42,
                    Low = 4.315,
                    Close = 4.40,
                    Volume = 22920
                }
            };

            Position position = new Position()
            {
                TickerSymbol = "TEST",
                Shares = 0,
                TradableCashBalance = 30000
            };

            position.IntraDayTrack = new TimeSeries(dataPoints1);
            position.Average = new SimpleMovingAverage(new double[]
            {
                0.01, 0.0040, 0.0039, 0.0038, 0.0038
            });
            yield return new object[] { position };

            position.IntraDayTrack = new TimeSeries(dataPoints2);
            position.Average = new SimpleMovingAverage(new double[]
            {
               4.83, 4.50, 4.48, 4.48, 4.47, 4.15
            });
            yield return new object[] { position };
        }
    }
}