﻿using Xunit;
using StockTraderBot;
using System;
using System.Collections.Generic;
using System.Text;
using MarketDataService;
using Moq;
using ExchangeService.Adapters;
using Serilog;
using Alpaca.Markets;
using System.Net.Http;
using System.Net;
using System.Text.Json;
using MarketDataService.Utilities;
using System.IO;
using StockTraderBot.Technical_Indicators;
using StockTraderBot.Models;

namespace SUT
{
    public class ApplicationTests
    {
        [Theory()]
        [InlineData("01/10/2020 09:29:00", "01/10/2020 09:30:00", "01/10/2020 16:00:00")]
        [InlineData("01/10/2020 23:30:00", "01/13/2020 09:30:00", "01/13/2020 16:00:00")]
        public void MarketCloseTest(string current, string nextOpen, string nextClose)
        {
            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();

            mockAppState.MarketHours = new Clock()
            {
                NextOpen = DateTime.Parse("01/10/2020 09:30:00"),
                NextClose = DateTime.Parse("01/10/2020 16:00:00")
            };

            mockExchange.Setup(x => x.GetMarketHours()).Returns(new Clock() { NextOpen = DateTime.Parse(nextOpen), NextClose = DateTime.Parse(nextClose) });

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            var time = DateTime.Parse(current);
            sut.CheckMarketHours(time);

            Assert.Equal(DateTime.Parse(nextOpen), sut.GetMarketHours().NextOpen);
            Assert.Equal(DateTime.Parse(nextClose), sut.GetMarketHours().NextClose);
        }

        [Fact()]
        public void MarketNearCloseTest()
        {
            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();
            var mockOrder = new Mock<IOrder>();

            var nearCloseTime = DateTime.Parse("01/13/2020 15:46:00");

            mockOrder.SetupGet(x => x.Symbol).Returns("TEST");
            mockOrder.SetupGet(x => x.Quantity).Returns(1000);
            mockOrder.SetupGet(x => x.CreatedAt).Returns(nearCloseTime);
            mockOrder.SetupGet(x => x.OrderSide).Returns(OrderSide.Sell);
            mockOrder.SetupGet(x => x.OrderStatus).Returns(OrderStatus.Filled);

            mockAppState.Positions = new List<Position>()
            {
                new Position()
                {
                    Shares = 1000,
                    TickerSymbol = "TEST"
                }
            };
            mockAppState.MarketHours = new Clock()
            {
                NextOpen = DateTime.Parse("01/13/2020 09:30:00"),
                NextClose = DateTime.Parse("01/13/2020 16:00:00")
            };

            mockExchange.Setup(x => x.GetMarketHours()).Returns(new Clock() { NextOpen = DateTime.Parse("01/13/2020 9:30:00"), NextClose = DateTime.Parse("01/13/2020 16:00:00") });
            mockExchange.Setup(x => x.SellStockAtMarket(mockAppState.Positions[0].TickerSymbol, mockAppState.Positions[0].Shares)).Returns(mockOrder.Object);

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            sut.CheckMarketHours(nearCloseTime);

            Assert.Empty(sut.GetWatchedPositions());
        }

        [Fact()]
        public void TopGainersTest()
        {
            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();

            mockMarketData.Setup(x => x.GetTopGainers()).Returns(new List<string>() { "TEST" });
            mockMarketData.Setup(x => x.GetStockIntraDay(It.IsAny<string>(), It.IsAny<bool>())).Returns(new TimeSeries(new List<Period>(), DateTime.Now));

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            sut.PopulatePositionsFromTopGainers();

            Assert.Single(sut.GetWatchedPositions());
            Assert.Equal("TEST", sut.GetWatchedPositions()[0].TickerSymbol);
        }

        [Fact()]
        public void CheckMarketHours()
        {
            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();


            mockAppState.MarketHours.NextOpen = DateTime.Parse("01/16/2020 9:30:00");
            mockAppState.MarketHours.NextClose = DateTime.Parse("01/16/2020 16:00:00");
            var clock = new Mock<IClock>();

            clock.SetupGet(x => x.NextOpen).Returns(DateTime.Parse("01/16/2020 9:30:00"));
            clock.SetupGet(x => x.NextClose).Returns(DateTime.Parse("01/16/2020 16:00:00"));

            mockExchange.Setup(x => x.GetMarketHours()).Returns(clock.Object);

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);

            var isOpen = sut.CheckMarketHours(DateTime.Parse("01/16/2020 10:00:00"));

            Assert.True(isOpen);
        }

        [Fact()]
        public void UpdateMarketDataForSessionTest()
        {
            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();

            string json = Helpers.ExtractJsonFromFile(@"Test Data\DNLI-test-data.json");
            var actualTimeseries = new TimeSeries(json);

            mockAppState.Positions.Add(new Position()
            {
                Name = "TEST",
                TickerSymbol = "DNLI",
                TradableCashBalance = 20000,
            });

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);
            sut.UpdateIntraDayData(actualTimeseries);
            var actualClose = sut.GetWatchedPositions()[0].IntraDayTrack.GetLatestPeriod().Close;

            Assert.Equal(19.4000, actualClose);
        }

        [Fact()]
        public void UpdateForNextSessionTest()
        {

            var mockMarketData = new Mock<IMarketDataAdapter>();
            var mockExchange = new Mock<IExchangeAdapter>();
            var mockAppState = new ApplicationState();
            var mockLogger = new Mock<ILogger>();
            var initalPositions = new List<Position>();
            var tickers = new List<string>() { "DNLI", "FTSV", "ICHBF", "LSCC", "SURRF" };

            mockMarketData.SetupSequence(x => x.GetStockIntraDay(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\DNLI-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\FTSV-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\ICHBF-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\LSCC-test-data.json")))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\SURRF-test-data.json")));

            for(var i = 0; i < 5; i++)
            {
                initalPositions.Add(new Position()
                {
                    Name = "TEST",
                    TickerSymbol = tickers[i],
                    TradableCashBalance = 20000,
                    Shares = 0
                });
            }

            mockAppState.Positions = initalPositions;

            var sut = new Application(mockExchange.Object, mockMarketData.Object, mockAppState, mockLogger.Object);
            sut.UpdateForSession(initalPositions);

            Assert.Equal(5, sut.GetWatchedPositions().Count);
            Assert.Equal(4.7500, sut.GetWatchedPositions()
                .Find(x => x.TickerSymbol == "ICHBF")
                .IntraDayTrack.GetLatestPeriod().Close);
        }
    }
}
