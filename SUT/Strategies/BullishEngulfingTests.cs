﻿using Xunit;
using StockTraderBot.Strategies;
using System;
using System.Collections.Generic;
using System.Text;
using Alpaca.Markets;
using MarketDataService;
using StockTraderBot.Technical_Indicators;
using Moq;
using SUT;

namespace StockTraderBot.Strategies.Tests
{
    public class BullishEngulfingTests
    {
        [Theory()]
        [MemberData(nameof(GetSampleNonPatternPositions))]
        public void BuyingRulesNoPatternTest(Position input)
        {
            var actualPosition = input;

            var strategy = new BullishEngulfing();

            Assert.False(strategy.BuyingRules(actualPosition));
        }

        [Theory()]
        [MemberData(nameof(GetSamplePatternPositions))]
        public void BuyingRulesPatternTest(Position input)
        {
            var strategy = new BullishEngulfing();

            Assert.True(strategy.BuyingRules(input));
        }

        [Theory()]
        [MemberData(nameof(GetSampleClosePrices))]
        public void SellingRulesTest(Position position, bool expectedOutput)
        {
            var strategy = new BullishEngulfing();
            Assert.Equal(expectedOutput, strategy.SellingRules(position));
        }

        public static IEnumerable<object[]> GetSampleClosePrices()
        {
            List<Period> dataPoints1 = new List<Period>()
            {
                new Period()
                {
                    Close = 5.7447
                },
                new Period()
                {
                    Close = 5.4940
                },
                new Period()
                {
                    Close = 5.2007
                }
            };

            List<Period> dataPoints2 = new List<Period>()
            {
                new Period()
                {
                    Close = 100.64
                },
                new Period()
                {
                    Close = 94.35
                },
                new Period()
                {
                    Close = 90.50
                }
            };

            List<Period> dataPoints3 = new List<Period>()
            {
                new Period()
                {
                    Close = 100.64
                },
                new Period()
                {
                    Close = 94.35
                },
                new Period()
                {
                    Close = 101.64
                },
                new Period()
                {
                    Close = 98.35
                },
                new Period()
                {
                    Close = 90.50
                }
            };

            Position position = new Position()
            {
                TickerSymbol = "TEST",
                Shares = 1000,
                TradableCashBalance = 1250000
            };

            //position.IntraDayTrack = new TimeSeries(dataPoints1);
            //position.TrailingStopPrice = 5.17023;

            //yield return new object[] { position, false };

            position.IntraDayTrack = new TimeSeries(dataPoints2);
            position.TrailingStopPrice = 90.576;

            yield return new object[] { position,  true};

            position.IntraDayTrack = new TimeSeries(dataPoints3);
            position.TrailingStopPrice = 91.48;

            yield return new object[] { position, true };


        }
        public static IEnumerable<object[]> GetSampleNonPatternPositions()
        {
            List<Period> dataPoints1 = new List<Period>()
            {
                new Period()
                {
                    Open = 5.6801,
                    High = 5.7889,
                    Low = 5.4243,
                    Close = 5.6447,
                    Volume = 2145
                },
                new Period()
                {
                    Open = 5.4940,
                    High = 5.4940,
                    Low = 5.4940,
                    Close = 5.4940,
                    Volume = 900
                },
                new Period()
                {
                    Open = 5.4901,
                    High = 5.5189,
                    Low = 5.4843,
                    Close = 5.5147,
                    Volume = 2145
                }
            };

            List<Period> dataPoints2 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.0736,
                    High = 4.1000,
                    Low = 4.0400,
                    Close = 4.0400,
                    Volume = 1300000
                },
                new Period()
                {
                    Open = 3.93,
                    High = 3.96,
                    Low = 3.84,
                    Close = 3.8627,
                    Volume = 1330000
                },
                new Period()
                {
                    Open = 3.94,
                    High = 4.00,
                    Low = 3.940,
                    Close = 3.9799,
                    Volume = 1340000
                }
            };

            List<Period> dataPoints3 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.4812,
                    High = 4.68,
                    Low = 4.480,
                    Close = 4.6793,
                    Volume = 3810000
                },
                new Period()
                {
                    Open = 4.0200,
                    High = 4.0300,
                    Low = 3.9700,
                    Close = 3.9800,
                    Volume = 4660000
                },
                new Period()
                {
                    Open = 4.82,
                    High = 4.85,
                    Low = 4.5700,
                    Close = 4.6550,
                    Volume = 3590000
                }
            };

            Position position = new Position()
            {
                TickerSymbol = "TEST",
                Shares = 0,
                TradableCashBalance = 3000000
            };

            position.IntraDayTrack = new TimeSeries(dataPoints1);
            position.Average = new SimpleMovingAverage(new double[]
            {
                5.78, 5.77, 5.77, 5.76, 5.77
            });
            yield return new object[] { position };

            position.IntraDayTrack = new TimeSeries(dataPoints2);
            position.Average = new SimpleMovingAverage(new double[]
            {
                4.15, 4.12, 4.11, 4.115, 4.120
            });
            yield return new object[] { position };

            position.IntraDayTrack = new TimeSeries(dataPoints3);
            position.Average = new SimpleMovingAverage(new double[]
            {
                4.4381, 4.40, 4.39, 4.35, 4.38
            });
            yield return new object[] { position };
        }

        public static IEnumerable<object[]> GetSamplePatternPositions()
        {
            List<Period> dataPoints1 = new List<Period>()
            {
                new Period()
                {
                    Open = 0.0039,
                    High = 0.0039,
                    Low = 0.0036,
                    Close = 0.0039,
                    Volume = 810000
                },
                new Period()
                {
                    Open = 0.0035,
                    High = 0.0035,
                    Low = 0.0033,
                    Close = 0.0033,
                    Volume = 412830
                },
                new Period()
                {
                    Open = 0.00331,
                    High = 0.0039,
                    Low = 0.0033,
                    Close = 0.0040,
                    Volume = 412830
                }
            };

            List<Period> dataPoints2 = new List<Period>()
            {
                new Period()
                {
                    Open = 4.41,
                    High = 4.41,
                    Low = 4.38,
                    Close = 4.39,
                    Volume = 378260
                },
                new Period()
                {
                    Open = 4.3450,
                    High = 4.36,
                    Low = 4.34,
                    Close = 4.315,
                    Volume = 8666260
                },
                new Period()
                {
                    Open = 4.3151,
                    High = 4.42,
                    Low = 4.315,
                    Close = 4.40,
                    Volume = 22920
                }
            };

            Position position = new Position()
            {
                TickerSymbol = "TEST",
                Shares = 0,
                TradableCashBalance = 3000000
            };

            position.IntraDayTrack = new TimeSeries(dataPoints1);
            position.Average = new SimpleMovingAverage(new double[]
            {
                0.01, 0.0040, 0.0039, 0.0038, 0.0038
            });
            yield return new object[] { position };

            position.IntraDayTrack = new TimeSeries(dataPoints2);
            position.Average = new SimpleMovingAverage(new double[]
            {
               4.83, 4.50, 4.48, 4.48, 4.47, 4.15
            });
            yield return new object[] { position };
        }

        private Mock<IMarketDataAdapter> MockBILLMarketData()
        {
            var mockData = new Mock<IMarketDataAdapter>();

            mockData.Setup(x => x.GetStockIntraDay(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(new TimeSeries(Helpers.ExtractJsonFromFile(@"Test Data\DNLI-test-data.json")));

            mockData.Setup(x => x.GetTopGainers()).Returns(new List<string>() { "BILL" });

            return mockData;
        }
    }
}